import mystack

def is_braces_sequence_correct(s:str):
    """
    Check a correct braces sequence with {,[,( braces
    :param s: string of symbols with different braces
    :return: TRUE or FALSE
    >>> is_braces_sequence_correct("((([()])))[]")
    True
    >>> is_braces_sequence_correct("((([()))[]")
    False
    >>> is_braces_sequence_correct("]")
    False
    >>> is_braces_sequence_correct("(")
    False
    >>> is_braces_sequence_correct("([]]")
    False
    """
    for brace in s:
        if brace not in "()[]{}":
            continue
        if brace in "([{":
            mystack.push(brace)
        else:
            assert brace in ")]}", "Expected closing brace: " + str(brace)
            if mystack.is_empty():
                return False
            left = mystack.pop()
            assert left in "([{", "Expected opening brace: " + str(brace)
            if left == "(":
                right = ")"
            elif left == "[":
                right = "]"
            elif left == "{":
                right = "}"
            if right != brace:
                return False
    return mystack.is_empty()

if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)
