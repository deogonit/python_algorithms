def dfs(vertex, G, used):
    # if used is None:
    #     used = set()
    # used = used or set()
    used.add(vertex)
    for neighbour in G[vertex]:
        if neighbour not in used:
            dfs(neighbour, G, used)


G = {
    'A': {'B'},
    'B': {'E'},
    'C': { 'D'},
    'D': {'C', 'H'},
    'E': {'A'},
    'F': {'G'},
    'G': {'F'},
    'H': {'D'}
}
used = set()
N = 0
for vertex in G:
    if vertex not in used:
        dfs(vertex, G, used)
        N += 1
print(N)
