from collections import deque


def add_edge(G, a, b, weight):
    for v1, v2 in (a, b), (b, a):
        if v1 not in G:
            G[v1] = {v2: weight}
        else:
            G[v1][v2] = weight


def read_graph():
    M = int(input())  # M - count of ribs
    G = {}
    for i in range(M):
        a, b, weight = input().split()
        weight = float(weight)
        add_edge(G, a, b, weight)
        add_edge(G, b, a, weight)


def dijkstra(G, start):
    distances = {vertex: None for vertex in G}
    distances[start] = 0
    queue = deque([start])
    while queue:
        cur_vertex = queue.popleft()
        for neighbour in G[cur_vertex]:
            if distances[neighbour] is None or (
                    distances[neighbour] > distances[cur_vertex] + G[cur_vertex][neighbour]):
                distances[neighbour] = distances[cur_vertex] + G[cur_vertex][neighbour]
                queue.append(neighbour)
    return distances


def restore_shortest_path(G, start, finish, shortest_distances):
    path = [finish]
    current_finish = finish
    while current_finish != start:
        for neighbour in G[current_finish]:
            if shortest_distances[current_finish] == (shortest_distances[neighbour] + G[neighbour][current_finish]):
                current_finish = neighbour
                path.append(neighbour)
                break
    path.reverse()
    return path


def interface():
    G = read_graph()
    start = input("Where vertex to start from?")
    while start not in G:
        start = input('There is no such vertex. '
                      'Where vertex to start from?')
    shortest_distances = dijkstra(G, start)
    finish = input("Where vertex to finish from?")
    while finish not in G:
        finish = input('There is no such vertex. '
                       'Where vertex to finish from?')
    shortest_path = restore_shortest_path(G, start, finish, shortest_distances)


def main():
    blank = [('a', 'b', 2), ('a', 'h', 15), ('b', 'c', 1), ('b', 'd', 5),
             ('c', 'g', 1), ('c', 'd', 3), ('c', 'f', 2), ('d', 'f', 4),
             ('d', 'e', 6), ('g', 'f', 1), ('f', 'h', 3), ('f', 'e', 7),
             ('e', 'i', 2), ('h', 'i', 12)]
    start = 'a'
    finish = 'h'
    graph = {}
    for i in blank:
        add_edge(graph, *i)
    shortest_distances = dijkstra(graph, start)
    shortest_path = restore_shortest_path(graph, start, finish, shortest_distances)
    for i in shortest_path:
        print(i)
    return shortest_path


if __name__ == "__main__":
    main()
