class Heap:
    def __init__(self):
        self.values = []
        self.size = 0

    def insert(self, x):
        self.values.append(x)
        self.size += 1
        self.sift_up(self.size - 1)

    def sift_up(self, i):
        while i != 0 and self.values[i] < self.values[(i - 1) // 2]:
            self.values[i], self.values[(i - 1) // 2] = \
                self.values[(i - 1) // 2], self.values[i]
            i = (i - 1) // 2

    def extract_min(self):
        if not self.size:
            return None
        tmp = self.values[0]
        self.values[0] = self.values[-1]
        self.values.pop()
        self.size -= 1
        self.sift_down(0)
        return tmp

    def sift_down(self, i):
        while 2 * i + 1 < self.size:
            j = i
            if self.values[2 * i + 1] < self.values[i]:
                j = 2 * i + 1
            if 2 * i + 2 < self.size and self.values[2 * i + 2] < self.values[j]:
                j = 2 * i + 2
            if i == j:
                break
            self.values[i],self.values[j] = self.values[j],self.values[i]
            i = j


def arrinheap(arr):
    heap = Heap()
    for i in range(len(arr)):
        heap.insert(arr[i])
    return get_sorted_array(heap)


def get_sorted_array(heap):
    arr = []
    while heap.size:
        arr.append(heap.extract_min())
    return arr


new_heap = Heap()

new_heap.insert(5)
new_heap.insert(6)
new_heap.insert(7)
new_heap.insert(8)
new_heap.insert(3)


mas = [1,53,623,2,45,562,62,12,41,4]
mas = arrinheap(mas)
print(mas)