import mystack

def reverse_polish_notation(rpn:list):
    """
    :param rpn: list of numbers and operations
    :return: result of rpn
    >>> reverse_polish_notation([3,4,'/'])
    0.75
    >>> reverse_polish_notation([3,4,'+'])
    7
    >>> reverse_polish_notation([3,4,'-'])
    -1
    >>> reverse_polish_notation([3,4,'*'])
    12
    >>> reverse_polish_notation([3,4,'^'])
    81
    """
    for token in rpn:
        if type(token) == int or type(token) == float:
            mystack.push(token)
        else:
            y = mystack.pop()
            x = mystack.pop()
            if token == '+':
                z = x + y
            elif token == '-':
                z = x - y
            elif token == '*':
                z = x * y
            elif token == '/':
                z = x / y
            elif token == '^':
                z = x ** y
            mystack.push(z)
    return mystack.pop()

if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)

